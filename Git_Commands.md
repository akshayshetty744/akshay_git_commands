    Q1)  what is version controls
    -->  Version control systems are software that help track changes made in code over time
    
    Q2)  What is distributed version control ?
    -->  Distributed version control system (DVCS) is a type of version control system like Git that replicates the repository onto each user’s machine that is each user has a self-contained first-class repository. The other type is Centralised Version Control (CVCS).
    
    Q3)  Alternatives to Git ?
    --> * Azure DevOps Server.
        * Helix Core.
        * AWS CodeCommit.
        * Subversion.
        * Rational ClearCase.
        * Plastic SCM.
        * Mercurial.
        * Micro Focus AccuRev.

    Q4)  What is Github ?
    -->  GitHub is a cloud-based publishing tool and hosting platform for individuals and teams using Git. In addition to making it easy for your team to store and work

    Q5)  Git vs GitHub ?
    -->  * Git is a version control system that allows developers to track changes in their code.
         * GitHub is a web-based hosting service for Git repositories. It makes Git more user-friendly  and also provides a platform for developers to share code with others. In addition, GitHub makes it easy for others to contribute to projects

    
    1)	How to check Git version
        git  --version

    2)	Git update or install
        sudo apt-get install git

    3)	Check all users in already exist
        git config  --global -e

    4)	Add new user name and email
        git config  --global user.name “Akshay shetty”
        git config  --global user.email shettyakshay744@gmail.com 

    5)	Get git help or all commands keys
        git config -h

    6)	Initialize git and get  into master branch, created one .git file 
        git init

    7)	How to .git file Remove
        rm  -rf .git

    8)  lists all the files that have to be committed.    
        git status

    9) the committed changes of master branch to your remote  repository.
        git push git@gitlab.com:akshayshetty744/akshay_git_commands.git    

    10) fetches and merges changes on the remote server to your working directory.
        git pull git@gitlab.com:akshayshetty744/akshay_git_commands.  

    11) list the version history for the current branch.    
        git log 

    12) obtain a repository from an existing URL.
        git clone git@gitlab.com:akshayshetty744/akshay_git_commands.    